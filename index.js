
// #3
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=> response.json())
.then((json)=> console.log(json));



// #4
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=> response.json())
.then((json) => {
	const arrayData = json.map((data)=>{
		return data.title;
	})
	console.log(arrayData);
});



// #5&6
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response)=> response.json())
.then((json)=> console.log(json));


// #7
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then((response)=> response.json())
.then((json)=> console.log(json));


// #8&9
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update my to do list with a different data structure.",
		completed: "pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then((response)=> response.json())
.then((json)=> console.log(json));

// #10
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: "false",
		dateCompleted: "07/09/21",
		status: "Complete"
	})
})
.then((response)=> response.json())
.then((json)=> console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})